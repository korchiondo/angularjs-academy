'use strict';

const modalComponent = require('./modalComponent'),
			ngMask = require('ngMask');

module.exports = angular.module('modal', ['ngMask'])
	.component('modalComponent', modalComponent);
