'use strict';

module.exports = {
	bindings: {
		account: '<',
		headerCaption: '@',
		buttonCaption: '@',
		onSubmit: '&'
	},
	controller: modalComponentController,
	templateUrl: 'js/components/modalComponent/modalComponent.html'
};

modalComponentController.$inject = ['$scope', '$element'];

function modalComponentController($scope, $element) {

	const vm = this,
				modalSelector = '.form-modal';

	vm.$onInit = function() {
		const formModal = $element.find(modalSelector);
		formModal.on('show.bs.modal', e => {
			vm.accountOrigin = angular.copy(vm.account || {});
			vm.account = angular.copy(vm.accountOrigin);
			$scope.$apply();
		});

		formModal.on('hide.bs.modal', e => {
			vm.account = vm.accountOrigin;
			vm.accountForm.$setPristine();
			vm.accountForm.$setUntouched();
		});
	};

	vm.getDataDismiss = () => vm.accountForm.$valid && 'modal' || '';

	vm.submit = () => {
		if(vm.accountForm.$valid) {
			vm.onSubmit({acc: vm.account});
		}
	};
}
