'use strict';

const deletePromptComponent = require('./deletePromptComponent');

module.exports = angular.module('deletePrompt', [])
	.component('deletePromptComponent', deletePromptComponent);
