'use strict';

module.exports = {
	bindings: {
		header: '@',
		content: '@',
		delete: '&'
	},
	controller: deletePromptComponentController,
	templateUrl: 'js/components/deletePromptComponent/deletePromptComponent.html'
};

function deletePromptComponentController() {

	const vm = this;

	vm.onDelete = function() {
		vm.delete();
	}
}
