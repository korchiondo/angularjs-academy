'use strict';

const userListController = require('./userListController'),
			userListService = require('./userListService'),
			modal = require('../../components/modalComponent');

module.exports = angular.module('userList', [modal.name])
	.controller('UserListController', userListController)
	.factory('userListService', userListService);
