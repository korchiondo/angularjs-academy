'use strict';

module.exports = /* @ngInject */ ($http, SERVER_URL) => {

	const api = {
		getAccounts: getAccounts,
		addNewAccount: addNewAccount
	};

	return api;

	function getAccounts() {
		return $http.get(SERVER_URL + 'account');
	}

	function addNewAccount(account) {
		return $http.post(SERVER_URL + 'account', account);
	}

};
