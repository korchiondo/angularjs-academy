'use strict';

module.exports = /* @ngInject */ function (userListService) {

	const vm = this;

	vm.$onInit = () => {
		fillAccounts();
	};

	vm.add = function(acc) {
		userListService.addNewAccount(acc).then(fillAccounts);
	};

	function fillAccounts() {
		userListService.getAccounts().then((response) => {
			vm.accounts = response.data;
		});
	}
};
