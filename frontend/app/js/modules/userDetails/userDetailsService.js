'use strict';

module.exports = /* @ngInject */ ($http, SERVER_URL) => {

	const api = {
		getAccount: getAccount,
		editAccount: editAccount,
		deleteAccount: deleteAccount,
		getSeriesData: getSeriesData,
		getChartConfig: getChartConfig
	};

	return api;

	function getAccount(id) {
		return $http.get(SERVER_URL + 'account/' + id);
	}

	function editAccount(account) {
		return $http.put(SERVER_URL + 'account/' + account.id, account);
	}

	function deleteAccount(id) {
		return $http.delete(SERVER_URL + 'account/' + id);
	}

	function getSeriesData(data) {
		return data.map(function(a) {
			return [new Date(a.date).getTime(), a.amountOfActions];
		});
	}

	function getChartConfig() {
		return {
			options: {
				chart: {
					type: 'line'
				},
				tooltip: {
					style: {
						padding: 10,
						fontWeight: 'bold'
					}
				}
			},
			title: {
				text: 'Activities'
			},
			loading: false,
			xAxis: {
				title: {text: 'Dates'},
				type: 'datetime',
				labels: {
					format: '{value:%Y-%m-%d}',
					rotation: 45,
					align: 'left'
				}
			},
			yAxis: {
				title: {text: 'Actions'}
			},

			useHighStocks: false,
			size: {
				width: 400,
				height: 300
			}
		};
	};
};
