'use strict';

const userDetailsController = require('./userDetailsController'),
			userDetailsService = require('./userDetailsService'),
			modal = require('../../components/modalComponent'),
			deletePrompt = require('../../components/deletePromptComponent'),
			highchartsNg = require('highcharts-ng');

module.exports = angular.module('userDetails', [
	modal.name,
	deletePrompt.name,
	highchartsNg])
		.controller('UserDetailsController', userDetailsController)
		.factory('userDetailsService', userDetailsService);
