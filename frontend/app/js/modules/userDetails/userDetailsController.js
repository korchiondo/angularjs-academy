'use strict';

module.exports = /* @ngInject */ function($stateParams, $location, userDetailsService) {

	const vm = this,
			accountId = $stateParams.id;

	vm.$onInit = () => {
		vm.chartConfig = userDetailsService.getChartConfig();
		getAccountData();
	}

	vm.delete = () => {
		userDetailsService.deleteAccount(vm.account.id).then(() => {
			$location.path('/');
		});
	};

	vm.edit = (acc) => {
		userDetailsService.editAccount(acc).then(getAccountData);
	};

	function getAccountData() {
		userDetailsService.getAccount(accountId).then((response) => {
			vm.chartConfig.series = [{
				data: userDetailsService.getSeriesData(response.data.activities)
			}]
			delete response.data.activities;
			vm.account = angular.copy(response.data);
		});
	}
};
