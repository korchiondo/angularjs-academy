'use strict';

module.exports = /* @ngInject */ ($stateProvider, $locationProvider) => {
	$stateProvider
		.state('home', {
			url: '/',
			templateUrl: 'js/modules/userList/user-list.html',
			controller: 'UserListController',
			controllerAs: 'listCtrl'
		})
		.state('details', {
			url: '/accounts/:id',
			templateUrl: 'js/modules/userDetails/user-details.html',
			controller: 'UserDetailsController',
			controllerAs: 'detailsCtrl'
		});
};
