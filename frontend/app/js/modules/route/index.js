'use strict';

const routeConfig = require('./routeConfig.js'),
			uiRouter =  require('angular-ui-router');

module.exports = angular.module('route', [uiRouter])
	.config(routeConfig);
