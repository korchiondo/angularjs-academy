'use strict';

const angular = require('angular'),
			route = require('./modules/route'),
			userList = require('./modules/userList'),
			userDetails = require('./modules/userDetails');

window.bootstrap = require('bootstrap');

module.exports = angular.module('app', [route.name, userList.name, userDetails.name])
	.constant(
		'SERVER_URL', 'http://localhost:1337/'
	).config(function ($httpProvider) {
		$httpProvider.useApplyAsync(true);
	});
