'use strict';

const gulp = require('gulp');
const templateCache = require('gulp-angular-templatecache');
const del = require('del');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const sourcemaps = require('gulp-sourcemaps');
const buffer = require('vinyl-buffer');
const babel = require('babelify');
const ngAnnotate = require('gulp-ng-annotate');
const inject = require('gulp-inject');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');

const src = './app';

const dist =  './dist';

const jsBundle = 'all.min.js';

const paths = {
	app: src + '/js/app.js',
	html: src + '/index.html',
	css: src + '/style/**/*.css',
	sass: src + '/style/*.scss',
	cssVendors: './node_modules/bootstrap/dist/css/bootstrap.min.css',
	scripts: [src + '/*.js', src + '/js/**/*.js'],
	tpl: [src + '/**/*.html', !src + '/index.html'],
	jsDist: dist + '/js',
	cssDist: dist + '/css'
};

gulp.task('clean-html', () => del([dist + '/index.html']));

gulp.task('build-html', ['clean-html'], () =>
	gulp.src(paths.html).pipe(gulp.dest(dist))
);

gulp.task('clean-css', () => del([paths.cssDist]));

gulp.task('sass', ['clean-css'], () =>
	gulp.src(paths.sass)
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest(paths.cssDist))
);

gulp.task('css-vendors', ['clean-css'], () =>
	gulp.src(paths.cssVendors)
		.pipe(gulp.dest(paths.cssDist))
);

gulp.task('minify-css', ['sass'], () =>
	gulp.src(paths.cssDist + '/base.css')
		.pipe(sourcemaps.init({loadMaps: true}))
		.pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest(paths.cssDist))
);

gulp.task('build-css', ['minify-css', 'css-vendors']);

gulp.task('clean-js', () => del([paths.jsDist]));

gulp.task('browserify', ['clean-js'], () => browserify({
		entries: [paths.app],
		debug: true
	})
	.transform("babelify", {presets: ["es2015"]})
	.bundle()
	.pipe(source('bundle.js'))
	.pipe(buffer())
	.pipe(ngAnnotate())
	.pipe(sourcemaps.init({loadMaps: true}))
	.pipe(uglify())
	.pipe(sourcemaps.write('./maps'))
	.pipe(gulp.dest(paths.jsDist))
);

gulp.task('tpls', ['clean-js'], () => gulp.src(paths.tpl)
	.pipe(templateCache('tpls.js', {module: 'app'}))
	.pipe(uglify())
	.pipe(gulp.dest(paths.jsDist))
);

gulp.task('concat-js', ['browserify', 'tpls'], () =>
		gulp.src(paths.jsDist + '/*.js')
			.pipe(concat(jsBundle))
			.pipe(gulp.dest(paths.jsDist))
);

gulp.task('build-js', ['concat-js'], () =>
	del.sync([paths.jsDist + '/*.js', '!' + paths.jsDist + '/' + jsBundle])
);

gulp.task('inject', ['build-html', 'build-js', 'build-css'], () => {
	const target = gulp.src(dist + '/index.html');
	const sources = gulp.src([dist + '/**/*.js', dist + '/**/*.css'], {read: false});
	return target.pipe(inject(sources, {relative: true})).pipe(gulp.dest('./dist'));
});

gulp.task('build', ['build-html', 'build-js', 'build-css', 'inject']);

gulp.task('default', function() {
	gulp.watch(src + '/**/*.js', ['build-js']);
	gulp.watch(src + '/**/*.scss', ['build-css']);
});
